/* auteur: Jérémy Bolloré 16/07/2019 */
/* MAJ: 05/10/2021*/

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "xintc.h"
#include "xil_printf.h"
#include "xbasic_types.h"
#include "xgpio.h"
#include "sleep.h"


// variables périphérique inclus dans le design
XGpio led;
XGpio signal;
XGpio marqueur;

// variable contrôleur d'interruption
static XIntc InterruptController;

/****Définitions des éléments pour la gestion des interruptions **************************************************/
#define INTC_DEVICE_ID      XPAR_INTC_0_DEVICE_ID       // identificateur du périphérique lié à l'interruption
#define INTC_DEVICE_INT_ID_1 XPAR_MICROBLAZE_0_AXI_INTC_SIGNAL_ENTRANT_IP2INTC_IRPT_INTR  
#define MASQUE 3									    // masque appliqué à l'	IP concat (multiplexeur interruption)
/*************Prototypes de fonction***********************************************************************************/
void Interruption(void *CallbackRef);
void InitInterruption(void);
void InitGpio(void);

int main()
{
	InitGpio();        // initialisation des périphériques entrées sorties
	InitInterruption(); // initialisation des interruptions
    init_platform();    // intialisation du périphérique UART

    print("programme autorisé \n\r");    // Affichage Tera Term
    while(1)
    {
    
    }

    cleanup_platform();
    return 0;
}

/*******************Routine d'interruption***************************************/

void Interruption(void *CallbackRef)
{


			XGpio_DiscreteWrite(&marqueur,1,1);
			XGpio_DiscreteWrite(&led,1,0xFFFF);
	    	usleep(2000);
	    	XGpio_DiscreteWrite(&led,1,0x0000);
	    	XGpio_DiscreteWrite(&marqueur,1,0);

	    	while(XGpio_DiscreteRead(&signal,1)==1); // la gestion des interruptions sur fronts montants ou descendants n'est pas possible avec le Microblaze.
	    	//Cette instruction permet d'assurer une interruption à chaque front montant du signal

		XGpio_InterruptClear(&signal, MASQUE);
}


/** fonction initialisation des interruptions**/

void InitInterruption(void)
{

	XIntc_Initialize(&InterruptController, INTC_DEVICE_ID); //Initialisation du pointeur d'interruption bouton

	/**** paramétrage interruption signal entrant****/

	XGpio_InterruptEnable(&signal, MASQUE); // masque d'interruption  valeur: 3
	XGpio_InterruptGlobalEnable(&signal);//Autorisation des interruption sur le broche en question




	// SetUpInterruptSystem(&InterruptController)
	{
		XIntc_Connect(&InterruptController,INTC_DEVICE_INT_ID_1, (XInterruptHandler)Interruption,(void *)0); // Connexion de l'ID avec l'interruption en question ( controleur d'interruption)
		XIntc_Start(&InterruptController, XIN_REAL_MODE);// autorise la sortie du controleur d'interruption vers le microprocesseur
		XIntc_Enable(&InterruptController,INTC_DEVICE_INT_ID_1 );//Autorise la source d'interruption (signal entrant)
		Xil_ExceptionInit();
		Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,(Xil_ExceptionHandler)XIntc_InterruptHandler,&InterruptController);
		Xil_ExceptionEnable();
	}



}

void InitGpio(void)
{
	XGpio_Initialize(&led, XPAR_LEDS_DEVICE_ID);   // initialisation du périphérique LEDS
	XGpio_Initialize(&signal, XPAR_SIGNAL_ENTRANT_DEVICE_ID);  // intialisation du périphérique Signal entrant 
	XGpio_Initialize(&marqueur, XPAR_MARQUEUR_DEVICE_ID);    // intitialisation du périphérique marqueur
}


