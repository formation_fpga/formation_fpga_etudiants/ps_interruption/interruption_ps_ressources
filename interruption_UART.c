/* auteur: J�r�my Bollor� 16/09/2020 */

/* MAJ: 04/10/2021/*/

#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "xintc.h"
#include "xil_printf.h"
#include "xbasic_types.h"
#include "xgpio.h"
#include "sleep.h"
#include <xuartlite_l.h>
#include <xintc_l.h>
#include "xuartlite.h"

#define UARTLITE_DEVICE_ID      XPAR_UARTLITE_1_DEVICE_ID


/***************************Variables*****************************/
XGpio led;
XUartLite UartLite;


/*************Prototypes de fonction***********************************************************************************/
void Init(void);						// Iniatialisation du mat�riel
void InterruptUART(void *baseaddr_p);  // INTERRUPTION UART

int main()
{
	Init();



    while(1)
    {
    	//programme principal
    }

    cleanup_platform();
    return 0;
}

/* routine d'interruption UART */
void  InterruptUART(void *baseaddr_p) {

	u8 c;																// d�claration de la variable caract�re
	while (!XUartLite_IsReceiveEmpty(XPAR_AXI_UARTLITE_0_BASEADDR)) {   // v�rifier si le buffer est plein
		/* Lecture d'un charact�re */
		c = XUartLite_RecvByte(XPAR_AXI_UARTLITE_0_BASEADDR);  		// la variable c prend pour variable l'octet re�u

		/* impression du charact�re sur l'hyperterminal  */
		if (c=='\r')												// saisie du caract�re entr�e
		xil_printf ("\n\r");										// impression retour � la ligne et retour chariot
		xil_printf ("%c", c);										// impression du charact�re tap�
		XUartLite_Send(&UartLite, &c, 1);							// impression du caract�re sur le port JB2


		XGpio_DiscreteWrite(&led,1,0xFFFFFFFF);						// Allumer les leds pendant 200 us
		usleep(200);
		XGpio_DiscreteWrite(&led,1,0x00000000);
	}
}

/* initialisation du syt�me */

void Init(void)
{

	XGpio_Initialize(&led, XPAR_LEDS_DEVICE_ID);   					// initialisation du pointeur de sortie
    init_platform(); 												// initialisation UARTLITE
    XUartLite_Initialize(&UartLite, UARTLITE_DEVICE_ID);			// initialisation UARTLITE 1

    print("Saisir du texte \n\r");


    microblaze_enable_interrupts();								   //Autorisation des interruption au niveau du microprocesseur

    XIntc_RegisterHandler(XPAR_INTC_0_BASEADDR,XPAR_MICROBLAZE_0_AXI_INTC_AXI_UARTLITE_0_INTERRUPT_INTR,(XInterruptHandler)InterruptUART,(void *)XPAR_AXI_UARTLITE_0_BASEADDR);// Connection de l'interruption UART � la routine d'interruption

    	XIntc_MasterEnable(XPAR_INTC_0_BASEADDR);                  //Contr�leur d'interruption activ�

    	XIntc_EnableIntr(XPAR_INTC_0_BASEADDR, XPAR_AXI_UARTLITE_0_INTERRUPT_MASK); //Autorisation de l'interruption au niveau du contr�leur d'interruption

    	XUartLite_EnableIntr(XPAR_AXI_UARTLITE_0_BASEADDR);			//Autorisation de l'interruption au niveau de l'IP UARTLITE
}


