#include <stdio.h>
#include "platform.h"
#include "xil_printf.h"
#include "xparameters.h"
#include "xintc.h"
#include "xil_printf.h"
#include "xbasic_types.h"
#include "xgpio.h"



XGpio btn;
XGpio led;

static XIntc InterruptController;

/***************************Variables*****************************/
Xuint32 data=0;

/****D�finitions des �l�ments **************************************************/

#define INTC_DEVICE_ID      XPAR_INTC_0_DEVICE_ID
#define INTC_DEVICE_INT_ID  XPAR_MICROBLAZE_0_AXI_INTC_BOUTONS_IP2INTC_IRPT_INTR
//#define BTN_INT             XGPIO_IR_CH1_MASK  // masquage des interruptions INT(0) 0x01
#define BTN_INT             1

/*************Prototypes de fonction***********************************************************************************/
void DeviceDriverHandler(void *CallbackRef);
void InitInterruption(void);
void InitGpio(void);

int main()
{
	InitGpio();
	InitInterruption();
    init_platform();

    print("Hello World\n\r");
    while(1)
    {

    }

    cleanup_platform();
    return 0;
}

/*******************Routine d'interruption***************************************/
void DeviceDriverHandler(void *CallbackRef)
{

/**************************************PARTIE A COMPLETER PAR L'ETUDIANT************************************************/
// ecrire la variable Data sur le terminal s�rie.
//cette variable doit s'incr�menter � chaque appui-bouton.
// le compteur est r�initialis� lorsque la valeur de Data =16.



	while (XGpio_DiscreteRead(&btn,1)==1); // pour �viter d'avoir une interruption sur les deux fronts
	XGpio_InterruptClear(&btn, BTN_INT);  // drapeau remis � 0 pour autoriser une nouvelle interruption
}

void InitInterruption(void)
{
	XGpio_InterruptEnable(&btn, BTN_INT); // masque d'interruption sur INT(0) valeur: 0x01
	XGpio_InterruptGlobalEnable(&btn);//Autorisation des interruption sur le broche en question

	XIntc_Initialize(&InterruptController, INTC_DEVICE_ID); //Initialisation du pointeur d'interruption

	// SetUpInterruptSystem(&InterruptController)
	{
		XIntc_Connect(&InterruptController,INTC_DEVICE_INT_ID, (XInterruptHandler)DeviceDriverHandler,(void *)0); // Connexion de l'ID avec l'interruption en question ( controleur d'interruption)
		XIntc_Start(&InterruptController, XIN_REAL_MODE);// autorise la sortie du controleur d'interruption vers le microprocesseur
		XIntc_Enable(&InterruptController,INTC_DEVICE_INT_ID );//Autorise la source d'interruption (bouton)
		Xil_ExceptionInit();
		Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,(Xil_ExceptionHandler)XIntc_InterruptHandler,&InterruptController);
		Xil_ExceptionEnable();
	}
}

void InitGpio(void)
{
	XGpio_Initialize(&btn, XPAR_BOUTONS_DEVICE_ID);  // intialisation du pointeur d'entr�e
	XGpio_Initialize(&led, XPAR_LEDS_DEVICE_ID);   // initialisation du pointeur de sortie
}






