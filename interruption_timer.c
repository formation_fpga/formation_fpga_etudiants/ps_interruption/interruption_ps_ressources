/*Création Jérémy Bolloré
MAJ : 05/10/2021*/



#include <stdio.h>
#include "platform.h"
#include "xparameters.h"
#include "xstatus.h"
#include "xintc.h"
#include "xil_exception.h"
#include "xtmrctr.h"
#include "xil_printf.h"
#include "xgpio.h"

// éléments de définition

#define INTC_DEVICE_ID		 	XPAR_INTC_0_DEVICE_ID         
#define INTC_DEVICE_INT_ID	 	XPAR_INTC_0_TMRCTR_0_VEC_ID   
#define TMRCTR_DEVICE_ID		XPAR_TMRCTR_0_DEVICE_ID
#define TIMER_CNTR_0	 		0
#define RESET_VALUE	 			0xFFFF0000         // Valeur permettant de définir la plage de comptage du TIMER


/* prototypes de fonction */

void InitialisationPeripherique(void);
int InitialisationTimer();
int SetUpInterruptSystem(XIntc *XIntcInstancePtr, XTmrCtr *XTmrInstancePtr);
void DeviceDriverHandler(void *CallbackRef);

/* déclaration variables interruptions et périphériques */

static XIntc InterruptController;
static XTmrCtr TimerCounterInst;
XGpio marqueur;

/* programme principal */

int main()
{
	InitialisationPeripherique(); // initialisation periphériques UART et marqueur
    xil_printf("Timer programme exemple \n\r");   // impression série sur Tera Term
    InitialisationTimer();   // intilisation des interruptions par le TIMER
	while (1)
	{
		XGpio_DiscreteWrite(&marqueur,1, 0);   // mise à 0 du marqueur
	}
    cleanup_platform();
    return 0;
}

/****** routine d'interruption TIMER***************/
void DeviceDriverHandler(void *CallbackRef)
{
	XTmrCtr_SetResetValue(&TimerCounterInst, TIMER_CNTR_0, RESET_VALUE);   // remise à 0 du timer
	XTmrCtr_Start(&TimerCounterInst, TIMER_CNTR_0);           // démarrage du timer 
	xil_printf("Evenement timer\n\r");  // impression série
}


/**initialisation périphériques*/
void InitialisationPeripherique(void)
{
	init_platform();   
	XGpio_Initialize(&marqueur,XPAR_MARQUEUR_DEVICE_ID); // initialisation périphérique marqueur
}

/** paramétrage TIMER***/
int InitialisationTimer()
{
	int Status;

XIntc_Initialize(&InterruptController, INTC_DEVICE_ID);
XIntc_SelfTest(&InterruptController);
XTmrCtr_Initialize(&TimerCounterInst, TMRCTR_DEVICE_ID);
XTmrCtr_SelfTest(&TimerCounterInst,TIMER_CNTR_0);
SetUpInterruptSystem(&InterruptController,&TimerCounterInst);

XTmrCtr_SetHandler(&TimerCounterInst,DeviceDriverHandler,&TimerCounterInst);
XTmrCtr_SetOptions(&TimerCounterInst, TIMER_CNTR_0, XTC_INT_MODE_OPTION);
XTmrCtr_SetResetValue(&TimerCounterInst, TIMER_CNTR_0, RESET_VALUE);
XTmrCtr_Start(&TimerCounterInst, TIMER_CNTR_0);

return 0;
}

int SetUpInterruptSystem(XIntc *XIntcInstancePtr, XTmrCtr *XTmrInstancePtr)
{


XIntc_Connect(XIntcInstancePtr, INTC_DEVICE_INT_ID,(XInterruptHandler)DeviceDriverHandler,(void *)XTmrInstancePtr);

XIntc_Start(XIntcInstancePtr, XIN_REAL_MODE);
XIntc_Enable(XIntcInstancePtr, INTC_DEVICE_INT_ID);

Xil_ExceptionInit();

Xil_ExceptionRegisterHandler(XIL_EXCEPTION_ID_INT,
					(Xil_ExceptionHandler)XIntc_InterruptHandler,
					XIntcInstancePtr);

Xil_ExceptionEnable();

return XST_SUCCESS;

}
